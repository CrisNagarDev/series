class Conexion {
    constructor() {
        this.init();
    }

    init() {
        this.xhr;

        this.data = {};

        this.start = 0;
        this.end = 0;
        this.listTV = [];

        this.tv = '';

        this.callback = () => { };

        this.xhr = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");

        this.xhr.addEventListener('loadstart', this.handleEvent);
        this.xhr.addEventListener('load', this.handleEvent);
        this.xhr.addEventListener('loadend', this.handleEvent);
        this.xhr.addEventListener('progress', this.handleEvent);
        this.xhr.addEventListener('error', this.handleEvent);
        this.xhr.addEventListener('abort', this.handleEvent);

        this.xhr.onreadystatechange = (e) => {
            if (this.xhr.readyState !== 4) {
                return;
            }

            if (this.xhr.status === 200) {
                this.data = JSON.parse(this.xhr.response);
                this.callback(JSON.parse(this.xhr.responseText));
            } else {
                console.warn('request_error');
            }
        };
    }

    handleEvent(e) {
        console.log(`${e.type}: ${e.loaded} bytes transferred\n`);
    }

    send(url) {
        //https://www.episodate.com/api/most-popular?page=2
        this.xhr.open('GET', url);

        this.xhr.send();
    }

    get list() {
        this.start = this.start === 0 ? 1 : this.start;

        for (let i = this.start; i <= this.end; i++) {
            this.send('https://www.episodate.com/api/most-popular?page=' + i);
            this.listTV.push(this.data);
        }

        return this.listTV;
    }

    get detail() {
        this.send('https://www.episodate.com/api/show-details?q=' + this.tv);

        return this.data.tvShow;
    }

    get data() {
        return this._data;
    }

    set data(responseData) {
        this._data = responseData;
    }
}