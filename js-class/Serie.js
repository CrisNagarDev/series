class Serie extends Conexion {
    constructor(tv) {
        super();
        this.tv = tv;
        this.raw = this.detail;
        this.show = {};
        this.callback = data => this.serieCallback(data.tvShow);
    }

    mapOBJ(s) {
        this.show.id = s.id
        this.show.name = s.name
        this.show.network = s.network
        this.show.country = s.country
        this.show.status = s.status
        this.show.start_date = s.start_date
        this.show.end_date = s.end_date
        this.show.permalink = s.permalink
        this.show.image_thumbail_path = s.image_thumbnail_path
    }

    serieCallback(data) {
        this.mapOBJ(data);
    }

    get showDetail() {
        return this.show;
    }
}