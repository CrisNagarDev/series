class MostPopular extends Conexion {
    constructor(start = 1, end = 912, limit = 0, isAll = false) {
        super();
        this.start = start;
        this.end = end;
        this.limit = (end * 20 < limit) || (limit === 0 && !isAll) ? end * 20 : limit;
        this.isAll = isAll;
        this.raw = this.list;
        this.as = []
        this.callback = data => this.mostPopularCallback(data);
    }

    mapOBJ(mptv) {
        console.log('map', mptv)
        for (let i = 0; i < mptv.length; i++) {
            
            for (let j = 0; j < mptv[i].tv_shows.length; j++) {
                if (this.as.length === this.limit && !this.isAll) {
                    return;
                }

                console.log('j', mptv[i].tv_shows[j])

                this.as.push(new Serie(mptv[i].tv_shows[j]).tvShow);

            }
        }
    }

    mostPopularCallback(data) {
        this.mapOBJ(data);
    }

    get all() {
        return this.as;
    }
}